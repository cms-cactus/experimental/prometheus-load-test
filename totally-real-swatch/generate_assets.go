//go:generate go run generate_assets.go

package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/shurcooL/vfsgen"
)

func main() {
	err := vfsgen.Generate(http.Dir("assets"), vfsgen.Options{
		PackageName:  "assets",
		BuildTags:    "production",
		VariableName: "Assets",
		Filename:     "src/assets/production.go",
	})
	checkErr(err)
}

func checkErr(err error) {
	if err != nil {
		fmt.Println("error while generating static assets:", err)
		os.Exit(1)
	}
}
