#/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t'

cd `dirname $0`

../swatch &
FOO_PID=$!
EXIT_STATUS=0
! go test *.go
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
  EXIT_STATUS=1
fi
kill $FOO_PID
exit $EXIT_STATUS