package testing

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"
)

func TestSum(t *testing.T) {
	// t.Errorf("some error %d", 5)
	resp, err := http.Get("http://localhost:3333/metrics")
	if err != nil {
		t.Errorf("can't get metrics: %s", err.Error())
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Errorf("can't get metrics body: %s", err.Error())
		return
	}

	fmt.Println(string(body))
}
