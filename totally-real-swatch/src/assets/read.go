package assets

// Read an asset and get byte buffer
func Read(path string) ([]byte, error) {
	f, err := Assets.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	fileinfo, err := f.Stat()
	if err != nil {
		return nil, err
	}
	size := fileinfo.Size()

	buffer := make([]byte, size)
	_, err = f.Read(buffer)
	if err != nil {
		return nil, err
	}
	return buffer, nil
}
