package main

import (
	"fmt"
	"net"
	"net/http"
	"os"

	Static "./assets"
)

func main() {
	if err := startServer(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func startServer() error {
	http.HandleFunc("/metrics", func(w http.ResponseWriter, r *http.Request) {
		if b, err := Static.Read("metrics"); err != nil {
			fmt.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("500 - Something bad happened!"))
		} else {
			fmt.Fprintf(w, string(b))
		}
	})

	listener, err := net.Listen("tcp", ":3333")
	if err != nil {
		return err
	}
	address := "http://" + listener.Addr().String()
	fmt.Println("listening on", address)
	return http.Serve(listener, nil)
}
