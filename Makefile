SHELL:=/bin/bash
.DEFAULT_GOAL := all

ROOT_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
SWATCH := $(ROOT_DIR)/totally-real-swatch

.PHONY: all
all: swatch

.PHONY: swatch
swatch:
	cd $(SWATCH) && $(MAKE)

.PHONY: clean
clean:
	cd $(SWATCH) && $(MAKE) clean